
{{- define "postgres.builtinCronJobs" -}}
postgres-backup:
  scheduling:
    schedule: "0 */22 * * *"
    concurrencyPolicy: "Forbid"
  volumes:
    postgres-backup:
      readOnly: false
      mountPath: /opt/app-root/postgres-backup/
  {{- $builtinPorts := include "postgres.builtinServicePorts" . | fromYaml }}
  {{- $mergedPorts := mustMergeOverwrite (dict) $builtinPorts .Values.servicePorts }}
  command: [bash, -c, "pg_dump -F t -f /opt/app-root/postgres-backup/${POSTGRESQL_DATABASE}.tar postgres://${POSTGRESQL_USER}:${POSTGRESQL_PASSWORD}@{{ .Values.baseName }}:{{ $mergedPorts.default.port }}/${POSTGRESQL_DATABASE}"]
{{- end }}
