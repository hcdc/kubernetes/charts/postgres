
{{- define "postgres.builtinVolumes" -}}
postgres:
  storage: "1Gi"
  mountPath: "/var/lib/pgsql/data"
  readOnly: false
postgres-backup:
  storage: "1Gi"
  readOnly: true
{{- end }}
