# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

image: python:3.9-alpine

stages:
  - lint
  - build
  - deploy

variables:
  GIT_DEPTH: 500

before_script:
  # install git
  - >-
    apk add --update --no-cache
    openssl
    git bash curl
  # install helm
  - curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  - VERSION=$(python scripts/print_version.py)
  - helm dependency update

lint:
  stage: lint
  script:
    - helm lint -f linter_values.yaml .

build:
  stage: build
  artifacts:
    paths:
      - postgres-*.tgz
    expire_in: 1 week
  script:
    - helm package --version ${VERSION} .

deploy-branch:
  stage: deploy
  only:
    - branches
  script:
    - >-
      curl --request POST
      --user gitlab-ci-token:$CI_JOB_TOKEN
      --form "chart=@postgres-${VERSION}.tgz"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/${CI_COMMIT_BRANCH}/charts"

deploy-stable:
  stage: deploy
  only:
    - tags
  script:
    - >-
      curl --request POST
      --user gitlab-ci-token:$CI_JOB_TOKEN
      --form "chart=@postgres-${VERSION}.tgz"
      "${CI_API_V4_URL}/projects/7205/packages/helm/api/stable/charts"

deploy-latest:
  stage: deploy
  only:
    - main
  script:
    - >-
      curl --request POST
      --user gitlab-ci-token:$CI_JOB_TOKEN
      --form "chart=@postgres-${VERSION}.tgz"
      "${CI_API_V4_URL}/projects/7205/packages/helm/api/latest/charts"
